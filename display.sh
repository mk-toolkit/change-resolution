#! /usr/bin/env bash

set -x

# 设置主屏幕和扩展屏幕的分辨率, 扩展是21:9的

#  xrandr
#  cvt 1920 1080
#  sudo cvt 2560 1080 60
  xrandr --newmode "1360x768_60.00"   84.75  1360 1432 1568 1776  768 771 781 798 -hsync +vsync
  xrandr --addmode "eDP-1" "1360x768_60.00"

  xrandr --newmode "2560x1080_60.00"  230.00  2560 2720 2992 3424  1080 1083 1093 1120 -hsync +vsync
  #sudo xrandr --addmode "HDMI-2" "2560x1080_60.00"
  xrandr --addmode "HDMI-2" "2560x1080_60.00"

  xrandr --output "HDMI-2" --mode "2560x1080_60.00"
  sleep 2
  xrandr --output "eDP-1" --mode "1360x768_60.00"

